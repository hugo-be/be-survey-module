import React, { Component } from 'react'

import ServiceRatingPage from 'survey_module'

export default class App extends Component {
  render () {
    return (
      <div>
        <ServiceRatingPage />
      </div>
    )
  }
}
