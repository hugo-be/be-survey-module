import React, { Component } from 'react';
var Survey = require('survey-react')
Survey.StylesManager.applyTheme("default");

var json = {
	questions: [
	{
		name: "name",
		type: "text",
		title: "Please enter your name:",
		placeHolder: "Jon Snow",
		isRequired: true
	}, {
		name: "birthdate",
		type: "text",
		inputType: "date",
		title: "Your birthdate:",
		isRequired: true
	}, {
		name: "color",
		type: "text",
		inputType: "color",
		title: "Your favorite color:"
	}, {
		name: "email",
		type: "text",
		inputType: "email",
		title: "Your e-mail:",
		placeHolder: "jon.snow@nightwatch.org",
		isRequired: true,
		validators: [
		{
			type: "email"
		}
		]
	}
	]
};



export default class ServiceRatingPage extends Component {

	constructor() {
		super();
		this.surveyModel = new Survey.Model(json);
	}
	componentDidMount() {

	}
	render() {
		return (
			<div className="mm-page">
				<h1> Survey Module Loaded! </h1>
				<Survey.Survey model={this.surveyModel}/>
			</div>
			);
	}
}
