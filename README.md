# survey_module

> Survey module

[![NPM](https://img.shields.io/npm/v/survey_module.svg)](https://www.npmjs.com/package/survey_module) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save survey_module
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'survey_module'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [](https://github.com/)
